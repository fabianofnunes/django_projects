from django.conf import settings
from django.conf.urls import url, include
from . import views
from django.conf.urls.static import static
from django.contrib.auth import views as django_views

urlpatterns = [
    url(r'^$', views.catalogo, name='catalogo'),
    url(r'^entrar$', django_views.login, {'template_name': 'gestorimagens/login.html'}, name='login'),
    url(r'^sair$', django_views.logout, {'next_page': 'gestorimagens:catalogo'}, name='logout'),
    url(r'^cadcategorias$', views.cadcategorias, name='cadastro_categorias'),
    url(r'^cadimagens$', views.cadimagens, name='cadastro_imagem'),
    url(r'^cadimagens/(?P<pk>\d+)$', views.cadimagens, name='cadastro_imagem'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]
