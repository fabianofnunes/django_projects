from django.contrib import admin
from django.contrib.auth.models import Group
from .models import Catalogo, Categoria


class CatalogoAdmin(admin.ModelAdmin):
    pass


class CategoriaAdmin(admin.ModelAdmin):
    pass

# Now register the new UserAdmin...
# ... and, since we're not using Django's built-in permissions,
# unregister the Group model from admin.

admin.site.register(Categoria, CategoriaAdmin)
admin.site.register(Catalogo, CatalogoAdmin)
