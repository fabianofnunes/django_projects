from django.apps import AppConfig


class GestorimagensConfig(AppConfig):
    name = 'gestorimagens'
