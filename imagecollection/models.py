from django.db import models


class Categoria(models.Model):
    descricao = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.descricao

    class Meta:
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categorias'


class Catalogo(models.Model):
    imagem = models.ImageField(
        verbose_name='Foto',
        upload_to='gestorimagens/uploads/originais',
    )
    nome = models.CharField(max_length=30)
    descricao = models.CharField(max_length=250)
    categoria = models.ForeignKey(
        Categoria, models.CASCADE,
        related_name='imgs',
    )
    filesize = models.IntegerField(
        blank=True,
        null=True
    )
    thumbnail = models.ImageField(
        upload_to='gestorimagens/uploads/thumbnails',
        max_length=500,
        blank=True,
        null=True
        )

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = 'Catalogo'
        verbose_name_plural = 'Catalogos'
