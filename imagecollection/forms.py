from django.forms import ModelForm
from .models import Catalogo, Categoria
from PIL import Image
import uuid


class CatalogoForm(ModelForm):

    def save(self, commit=True):
        catalogo_item = super(CatalogoForm, self).save()
        catalogo_item.filesize = self.cleaned_data['imagem'].size
        extensao = catalogo_item.imagem.name.split('.')[-1]
        filename = "%s.%s" % (uuid.uuid4(), extensao)
        catalogo_item.thumbnail = 'gestorimagens/uploads/thumbnails/%s' % (filename)
        miniatura = Image.open(catalogo_item.imagem.path)
        miniatura.thumbnail((500, 500), Image.ANTIALIAS)
        miniatura.save(catalogo_item.thumbnail.path)

        if commit:
            catalogo_item.save()
        return catalogo_item

    class Meta:
        model = Catalogo
        fields = ['imagem', 'nome', 'descricao', 'categoria', ]


class CategoriaForm(ModelForm):

    class Meta:
        model = Categoria
        fields = ['descricao']
