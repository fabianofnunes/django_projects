from django.shortcuts import render, get_object_or_404
from .models import Catalogo, Categoria
from .forms import CatalogoForm, CategoriaForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from datetime import datetime
from zigoeduadm import settings
import os
from django.contrib.auth.decorators import login_required


@login_required
def cadcategorias(request):
    template_name = 'gestorimagens/cadcategorias.html'
    context = {}
    form = CategoriaForm(request.POST or None)
    if form.is_valid():
        form.save()
        context['success'] = True
        form = CategoriaForm()
    context['form'] = form
    return render(request, template_name, context)


@login_required
def cadimagens(request, pk=None):
    template_name = "gestorimagens/cadimagens.html"
    context = {}
    imagem_atual = pk
    if pk:
        imagem_atual = get_object_or_404(Catalogo, pk=pk)
        context['imagem_atual'] = os.path.join(settings.MEDIA_URL, 'imagens', str(imagem_atual.thumbnail))
    else:
        context['imagem_atual'] = os.path.join('gestorimagens/uploads/placeholder.jpg')

    form = CatalogoForm(request.POST, request.FILES, instance=imagem_atual)
    if request.POST:
        if form.is_valid():
            imagem_atual = form.save()
            context['success'] = True
            context['imagem_atual'] = os.path.join('gestorimagens/uploads/placeholder.jpg')
            form = CatalogoForm()
    else:
        form = CatalogoForm(instance=imagem_atual)

    context['form'] = form
    return render(request, template_name, context)


@login_required
def catalogo(request):
    template_name = 'gestorimagens/catalogoimagens.html'
    context = {}
    categorias = Categoria.objects.all()
    catforfilter = request.GET.get('filtrocat')

    if catforfilter:
        imagens_all = Catalogo.objects.filter(categoria=catforfilter)
    else:
        catforfilter = 0
        imagens_all = Catalogo.objects.all()

    paginator = Paginator(imagens_all, 6)
    page = request.GET.get('page')

    try:
        imagens = paginator.page(page)
    except PageNotAnInteger:
        imagens = paginator.page(1)
    except EmptyPage:
        imagens = paginator.page(paginator.num_pages)

    context = {
            'categorias': categorias,
            'imagens': imagens,
            'num_rows': range(0, 3),
            'dataatual': datetime.now(),
            'catforfilter': int(catforfilter),
        }

    return render(request, template_name, context)
