from django.db import models


class Regioes(models.Model):
    nome = models.CharField(max_length=100)

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = 'Região Brasileira'
        verbose_name_plural = 'Regiões Brasileira'


class Estados(models.Model):
    regiao = models.ForeignKey(Regioes, models.CASCADE, related_name='estados')
    nome = models.CharField(max_length=100)
    sigla = models.CharField(max_length=2)

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = 'Estado'
        verbose_name_plural = 'Estados'


class Cidades(models.Model):
    estado = models.ForeignKey(Estados, models.CASCADE, related_name='cidades')
    nome = models.CharField(max_length=100)

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = 'Cidade'
        verbose_name_plural = 'Cidades'


class PlanoAssinatura(models.Model):
    descricao = models.CharField(verbose_name='Descrição', max_length=100)

    def __str__(self):
        return self.descricao

    class Meta:
        verbose_name = 'Plano'
        verbose_name_plural = 'Planos'
