from django.contrib import admin
from twosides.mensagens.models import Mensagem


class MensagemAdmin(admin.ModelAdmin):
    list_display = (
        'assunto',
        'mensagem',
        'data',
        'de',
        'para',
        'created_at',
        'modified_at',
    )

# Register your models here.
admin.site.register(Mensagem, MensagemAdmin)
